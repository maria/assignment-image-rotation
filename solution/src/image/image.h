#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>
#include <stdint.h>


struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
}  __attribute__((packed));

bool new_image(uint32_t width, uint32_t height, struct image *structimage);

void delete_image(struct image* img);


#endif //IMAGE_H
