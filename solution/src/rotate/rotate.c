#include "../image/image.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

bool image_rotation(struct image const source, struct image *outim){
    if(!new_image(source.height, source.width, outim)){
        return false;
    }
    for (size_t i = 0; i < source.height; i++ ) {
        for (size_t j = 0; j < source.width; j++) {
            outim->data[j * source.height + (source.height - 1 - i)] = source.data[i * source.width + j];
        }
    }
    return true;
}

