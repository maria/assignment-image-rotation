#include "image.h"
#include <stdlib.h>
void delete_image(struct image* img) {
    if(img->data){
        free(img->data);
    }
}

bool new_image(uint32_t width, uint32_t height, struct image *structimage){
    structimage->width = width;
    structimage->height = height;
    structimage->data = malloc(sizeof(struct pixel) * width * height);
    return structimage->data != NULL;
}

