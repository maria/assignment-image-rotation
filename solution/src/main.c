#include "bmp/bmp.h"
#include "image/image.h"
#include "rotate/rotate.h"
#include <stdio.h>

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "USAGE: ./image-transformer <source-image> <transformed-image>\n");
        return 1;
    }

    FILE *f = fopen(argv[1], "rb");
    if(!f){
        fprintf(stderr, "Cant open input file\n");
        fclose(f);
        return 1;
    }

    struct image img;

    enum read_status stat = from_bmp(f, &img);
    fclose(f);

    if(stat != READ_OK){
        fprintf(stderr, "Cant read input file");
        delete_image(&img);
        return 1;
    }

    struct image output_img;
    if(!image_rotation(img, &output_img)){
        fprintf(stderr, "Cant rotate your image\n");
        delete_image(&img);
        delete_image(&output_img);
        return 1;
    }
    delete_image(&img);

    FILE *outf = fopen(argv[2], "wb");
    if(!outf){
        fprintf(stderr, "Cant open your output file\n");
        fclose(outf);
        return 1;
    }
    if(to_bmp(outf, &output_img) != WRITE_OK){
        fprintf(stderr, "Cant write to your output file");
        delete_image(&output_img);
        fclose(outf);
        return 1;
    }

    delete_image(&output_img);
    fclose(outf);

    fprintf(stdout, "Image rotated successfully");

    return 0;
}

